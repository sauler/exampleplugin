/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of ExamplePlugin for AQQ                                  *
 *                                                                             *
 * ExamplePlugin plugin is free software: you can redistribute it and/or       *
 * modify it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * ExamplePlugin is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "EventHandler.h"
#include "SDK\PluginLink.h"
#include "SDK\Paths.h"
#include "SDK\AQQ.h"

CEventHandler::CEventHandler()
{
	CPluginLink::instance()->GetLink().HookEvent(AQQ_SYSTEM_MODULESLOADED,
		ModulesLoaded);
}

CEventHandler::~CEventHandler()
{
	CPluginLink::instance()->GetLink().UnhookEvent(ModulesLoaded);

}

INT_PTR __stdcall CEventHandler::ModulesLoaded(WPARAM wParam, LPARAM lParam)
{
	AQQ::Functions::ShowMessage(0, "App path: " + CPaths::instance()->AppPath());
	AQQ::Functions::ShowMessage(0, "Plugin directory: " + CPaths::instance()->PluginUserDir());
	AQQ::Functions::ShowMessage(0, "Theme directory: " + CPaths::instance()->ThemeDir());

	return 0;
}
